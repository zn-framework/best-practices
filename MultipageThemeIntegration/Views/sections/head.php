<?php

Import::theme
([
    'Airspace/css/owl.carousel.css',
    'Airspace/css/bootstrap.min.css',
    'Airspace/css/font-awesome.min.css',
    'Airspace/css/style.css',
    'Airspace/css/ionicons.min.css',
    'Airspace/css/animate.css',
    'Airspace/css/responsive.css',
    'Airspace/js/vendor/modernizr-2.6.2.min.js',
    'Airspace/js/vendor/jquery-1.10.2.min.js',
    'Airspace/s/bootstrap.min.js',
    'Airspace/js/owl.carousel.min.js',
    'Airspace/js/plugins.js',
    'Airspace/js/min/waypoints.min.js',
    'Airspace/js/jquery.counterup.js',
    'Airspace/js/google-map-init.js',
    'Airspace/js/main.js'
]);

Import::script('https://maps.googleapis.com/maps/api/js');
