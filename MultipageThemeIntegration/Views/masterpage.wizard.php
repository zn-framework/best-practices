<!-- Projects/Frontend/Config/Masterpage.php -> Body Page -->

@Import::view('sections/header'):

@if( isset($slider) ):
    @Import::view('sections/slider'):
@endif:

@Import::view($page):

@Import::view('sections/footer'):
