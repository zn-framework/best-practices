<?php namespace Project\Controllers;

class Contact extends Controller
{
    public function main(String $params = NULL)
    {
        // Simplicity is our choice, how about yours ?

        $this->masterpage->title = 'Contact';
        $this->masterpage->page  = 'contact.wizard';
        $this->masterpage->theme =
        [
            'name' => ['Airspace/js/google-map-init.js']
        ];
    }
}
