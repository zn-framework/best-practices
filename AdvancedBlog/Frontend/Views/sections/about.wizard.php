{[ $about = ZN::blog()->rowSlug('about-me'); ]}
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="block wow fadeInLeft" data-wow-delay=".3s" data-wow-duration="500ms">
                    <h2>
                    {{Strings::upperCase($about->name)}}
                    </h2>

                    <p>
                    {{$about->content}}
                    </p>
                </div>

            </div>
            <div class="col-md-6 col-sm-6">
                <div class="block wow fadeInRight" data-wow-delay=".3s" data-wow-duration="500ms">
                    <img src="{{blogImage($about->image)}}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
