<section class="global-page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2>{{Strings::upperCase(CURRENT_CONTROLLER)}}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{siteUrl()}}">
                                <i class="ion-ios-home"></i>
                                Home
                            </a>
                        </li>
                        <li class="active">{{Strings::titleCase(CURRENT_CONTROLLER)}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
