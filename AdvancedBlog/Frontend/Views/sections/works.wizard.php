{[ $blogs = ZN::blog()->resultByCategorySlug('works'); ]}
<section id="works" class="works">
    <div class="container">
        <div class="section-heading">
            <h1 class="title wow fadeInDown" data-wow-delay=".3s">Latest Works</h1>
            <p class="wow fadeInDown" data-wow-delay=".5s">
                Aliquam lobortis. Maecenas vestibulum mollis diam. Pellentesque auctor neque nec urna. Nulla sit amet est. Aenean posuere <br> tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus.
            </p>
        </div>
        <div class="row">

            @foreach( $blogs as $blog ):
            <div class="col-sm-4 col-xs-12">
                <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                    <div class="img-wrapper">
                        <img src="{{blogImage($blog->image)}}" class="img-responsive" alt="this is a title" >
                        <div class="overlay">
                            <div class="buttons">
                                <a rel="gallery" class="fancybox" href="{{blogImage($blog->image)}}">Demo</a>
                                <a target="_blank" href="{{blogDetail($blog->id)}}">Details</a>
                            </div>
                        </div>
                    </div>
                    <figcaption>
                    <h4>
                    <a href="{{blogDetail($blog->id)}}">
                        {{$blog->name}}
                    </a>
                    </h4>
                    <p>
                        {{Limiter::char($blog->content, 100)}}
                    </p>
                    </figcaption>
                </figure>
            </div>
            @endforeach:
        </div>
    </div>
</section>
