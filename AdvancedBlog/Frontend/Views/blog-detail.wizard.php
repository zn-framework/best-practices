<section class="global-page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2>Big Headline for Blog Post</h2>
                    <div class="portfolio-meta">
                        <span>Dec 11, 2020</span>|
                        <span> Category: typography</span>|
                        <span> Tags: <a href="">business</a>,<a href="">people</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section><!--/#Page header-->
    <section class="single-post">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="post-img">
                        <img class="img-responsive" alt="" src="{{blogImage($blog->image)}}">
                    </div>
                    <div class="post-content">
                        <p>
                            {{$blog->content}}
                        </p>
                    </div>
                    <ul class="social-share">
                        <h4>Share this article</h4>
                        <li>
                            <a href="#" class="Facebook">
                                <i class="ion-social-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="Twitter">
                                <i class="ion-social-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="Linkedin">
                                <i class="ion-social-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="Google Plus">
                                <i class="ion-social-googleplus"></i>
                            </a>
                        </li>

                    </ul>

                </div>
            </div>
        
        </div>
    </section>
