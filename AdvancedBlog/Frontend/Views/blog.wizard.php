<section id="blog-full-width">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="sidebar">
                    <div class="search widget">
                        <form action="" method="get" class="searchform" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"> <i class="ion-search"></i> </button>
                                </span>
                                </div><!-- /input-group -->
                            </form>
                        </div>
                        <div class="author widget">
                            <img class="img-responsive" src="{{THEMES_URL}}images/author/author-bg.jpg">
                            <div class="author-body text-center">
                                <div class="author-img">
                                    <img src="{{THEMES_URL}}images/author/author.jpg">
                                </div>
                                <div class="author-bio">
                                    <h3>Jonathon Andrew</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt magnam asperiores consectetur, corporis ullam impedit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="categories widget">
                            <h3 class="widget-head">Categories</h3>
                            <ul>
                                @foreach( $categories as $category ):
                                <li>
                                    <a href="{{siteUrl('blog/' . $category->slug)}}">{{$category->name}}</a> <span class="badge">{{ZN::category()->blogCountById($category->id)}}</span>
                                </li>
                                @endforeach:
                            </ul>
                        </div>

                        <div class="recent-post widget">
                            <h3>Recent Posts</h3>
                            <ul>
                                @foreach( $recentPost as $post ):
                                <li>
                                    <a href="{{blogDetail($post->id)}}">{{$post->name}}</a><br>
                                    <time>{{$post->date}}</time>
                                </li>
                                @endforeach:
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">

                    @foreach( $blogs as $blog ):
                    <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                        <div class="blog-post-image">
                            <a href="post-fullwidth.html"><img class="img-responsive" src="{{blogImage($blog->image)}}" alt="" /></a>
                        </div>
                        <div class="blog-content">
                            <h2 class="blogpost-title">
                            <a href="{{blogDetail($blog->id)}}">{{$blog->name}}</a>
                            </h2>
                            <div class="blog-meta">
                                <span>{{$blog->date}}</span>
                                <span>by <a href="">Admin</a></span>
                                <span><a href="">business</a>,<a href="">people</a></span>
                            </div>
                            <p>
                                {{Limiter::word($blog->content, 100)}}
                            </p>
                            <a href="{{blogDetail($blog->id)}}" class="btn btn-dafault btn-details">Continue Reading</a>
                        </div>
                    </article>
                    @endforeach:

                </div>
            </div>
        </section>
