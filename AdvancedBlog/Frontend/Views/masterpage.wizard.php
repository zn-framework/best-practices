<!-- Projects/Frontend/Config/Masterpage.php -> Body Page -->

@Import::view('sections/header.wizard'):

@if( isset($slider) ):
    @Import::view('sections/slider.wizard'):
@endif:

@if( isset($global) ):
    @Import::view('sections/global.wizard'):
@endif:

{{$view ?? NULL}}

@if( isset($about) ):
    @Import::view('sections/about.wizard'):
@endif:

@if( isset($works) ):
    @Import::view('sections/works.wizard'):
@endif:

@if( isset($feature) ):
    @Import::view('sections/feature.wizard'):
@endif:

@if( isset($action) ):
    @Import::view('sections/action.wizard'):
@endif:

@Import::view('sections/footer.wizard'):
