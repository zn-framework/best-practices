<?php
namespace Project\Controllers;

class home extends Controller
{
	public function main()
	{
		$this->masterpage->title   = 'My Blog';
		$this->masterpage->slider  = true;
		$this->masterpage->about   = true;
		$this->masterpage->works   = true;
		$this->masterpage->feature = true;
		$this->masterpage->action  = true;
	}

	public function s404()
	{
		$this->masterpage->title   = '404 Not Found!';
		$this->masterpage->action  = true;
	}
}
