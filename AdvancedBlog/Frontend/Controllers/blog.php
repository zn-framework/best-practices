<?php
namespace Project\Controllers;

use Arrays;

class blog extends Controller
{
	public function main($slug = NULL)
	{
		$this->masterpage->title   = 'Blog';
		$this->masterpage->global  = true;
		$this->masterpage->action  = true;

		$this->view->categories = $this->category->result();
		$this->view->recentPost = $this->blog->recentPost(5);

		$this->view->blogs = $this->blog->resultByCategorySlug($slug ?? 'blogs');
	}

	public function detail(String $detail)
	{
		$blog = $this->blog->rowId(blogId($detail));

		$this->masterpage->title  = $blog->name;
		$this->masterpage->action = true;

		$this->view->blog = $blog;
	}

	public function category()
	{
		// Your codes...
	}
}
