<?php namespace Project\Controllers;

use Config, DB, File;

class Initialize extends Controller
{
    public function main()
    {
        DB::query(File::read(FILES_DIR . 'test.sql'));

        Config::set('Masterpage',
        [
            'script' => 'jquery',
            'theme'  =>
            [
                'name' =>
                [
                    'css/bootstrap.min.css',
                    'css/ionicons.min.css',
                    'css/animate.css',
                    'css/slider.css',
                    'css/owl.carousel.css',
                    'css/owl.theme.css',
                    'css/jquery.fancybox.css',
                    'css/main.css',
                    'css/responsive.css',
                    'js/vendor/modernizr-2.6.2.min.js',
                    'js/owl.carousel.min.js',
                    'js/bootstrap.min.js',
                    'js/wow.min.js',
                    'js/slider.js',
                    'js/jquery.fancybox.js',
                    'js/main.js'
                ]
            ]
        ]);
    }
}
