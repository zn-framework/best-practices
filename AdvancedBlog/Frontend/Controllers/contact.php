<?php
namespace Project\Controllers;

use Method;

class contact extends Controller
{
	public function main()
	{
		if( Method::post('send') )
		{
			Method::post('ip', ipv4());

			if( $this->ticket->insert('post') )
			{
				$this->view->success = 'Your message has been successfully sent.';
			}
			else
			{
				$this->view->error   = 'Your message could not be sent. Try again later!';
			}
		}

		$this->masterpage->title   = 'Contact';
		$this->masterpage->global  = true;
		$this->masterpage->action  = true;
	}
}
