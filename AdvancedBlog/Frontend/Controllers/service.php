<?php
namespace Project\Controllers;

class service extends Controller
{
	public function main()
	{
		$this->masterpage->title   = 'Service';
		$this->masterpage->global  = true;
		$this->masterpage->action  = true;

		$this->view->result = $this->blog->resultByCategorySlug('works');
	}
}
