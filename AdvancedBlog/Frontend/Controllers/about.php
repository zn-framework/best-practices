<?php
namespace Project\Controllers;

class about extends Controller
{
	public function main()
	{
		$this->masterpage->title   = 'About Me';
		$this->masterpage->global  = true;
		$this->masterpage->action  = true;
	}
}
