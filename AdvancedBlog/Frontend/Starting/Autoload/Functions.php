<?php

function returnInfo($success, $error)
{
    $str = NULL;

    if( ! empty($success) ):
    $str .= '<div class="form-group">
        <div class="alert alert-success">
          <strong>Success!</strong> ' . $success . '
        </div>
    </div>';
    endif;

    if( ! empty($error) ):
        $str .= '<div class="form-group">
            <div class="alert alert-danger">
              <strong>Success!</strong> ' . $error . '
            </div>
        </div>';
    endif;

    return $str;
}

function blogImage($image)
{
    if( isUrl($image) )
    {
        return $image;
    }

    return THEMES_URL . 'images/' . $image;
}

function blogDetail($id)
{
    $blog = ZN::blog()->rowId($id);

    return siteUrl('blog/detail/' . $blog->slug . '-' . $blog->id);
}

function blogId($uri)
{
    return divide($uri, '-', -1);
}
