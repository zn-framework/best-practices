<?php
class Blog extends GrandModel
{
    public function resultWithCategory()
    {
        return $this->select('category.name as categoryName', 'blog.*')
                    ->leftJoin('category.id', 'blog.category_id')
                    ->result();
    }

    public function resultByCategorySlug($categorySlug)
    {
        return $this->select('category.name as categoryName', 'blog.*')
                    ->leftJoin('category.id', 'blog.category_id')
                    ->where('category.slug', $categorySlug)
                    ->result();
    }

    public function recentPost($limit)
    {
        return $this->orderBy('date', 'desc')->limit($limit)->result();
    }
}
