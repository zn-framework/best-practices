<?php
class InternalMigrateblog extends ZN\Database\InternalMigration
{
	//--------------------------------------------------------------------------------------------------------
	// Class/Table Name
	//--------------------------------------------------------------------------------------------------------
	const table = __CLASS__;

	//--------------------------------------------------------------------------------------------------------
	// Up
	//--------------------------------------------------------------------------------------------------------
	public function up()
	{
		// Default Query
		return $this->createTable
		([
			'id' 			=> [DB::int(11), DB::primaryKey(), DB::autoIncrement()],
			'category_id'	=> [DB::int(11)],
			'name'			=> [DB::varchar(200)],
			'slug'			=> [DB::varchar(255)],
			'content'		=> [DB::longtext()],
			'view_count'	=> [DB::int(11)],
			'image'         => [DB::text()],
			'date'			=> [DB::datetime()],
			'reply_date'	=> [DB::datetime()]
		], DB::encoding());
	}

	//--------------------------------------------------------------------------------------------------------
	// Down
	//--------------------------------------------------------------------------------------------------------
	public function down()
	{
		// Default Query
		$this->dropTable();
	}
}