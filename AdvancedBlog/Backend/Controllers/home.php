<?php namespace Project\Controllers;

class Home extends Controller
{
    public function main(String $params = NULL)
    {
        // Simplicity is our choice, how about yours ?

        $this->masterpage->title = 'Admin Panel';


        $this->view->blogCount      = $this->blog->totalRows();
        $this->view->categoryCount  = $this->category->totalRows();
        $this->view->ticketCount    = $this->ticket->totalRows();
    }
}
