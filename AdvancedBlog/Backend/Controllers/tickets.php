<?php namespace Project\Controllers;

class Tickets extends Controller
{
    public function main(String $params = NULL)
    {
        // Simplicity is our choice, how about yours ?
        $this->masterpage->title = 'Tickets';
        $this->masterpage->plugin['name'] =
        [
            'vendor/datatables-plugins/dataTables.bootstrap.css',
            'vendor/datatables-responsive/dataTables.responsive.css',
            'vendor/datatables/js/jquery.dataTables.min.js',
            'vendor/datatables-plugins/dataTables.bootstrap.min.js',
            'vendor/datatables-responsive/dataTables.responsive.js'
        ];

        $this->view->result = $this->ticket->result();
    }

    public function delete(Int $id)
    {
        $this->ticket->deleteId($id);

        redirect( prevUrl() );
    }
}
