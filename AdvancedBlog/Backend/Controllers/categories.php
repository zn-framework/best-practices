<?php namespace Project\Controllers;

use Method, Date, Converter;

class Categories extends Controller
{
    public function main(String $params = NULL)
    {
        // Simplicity is our choice, how about yours ?
        $this->masterpage->title = 'Categories';
        $this->masterpage->plugin['name'] =
        [
            'vendor/datatables-plugins/dataTables.bootstrap.css',
            'vendor/datatables-responsive/dataTables.responsive.css',
            'vendor/datatables/js/jquery.dataTables.min.js',
            'vendor/datatables-plugins/dataTables.bootstrap.min.js',
            'vendor/datatables-responsive/dataTables.responsive.js'
        ];

        $this->view->result = $this->category->result();
    }

    public function add(String $params = NULL)
    {
        if( Method::post('create') )
        {
            Method::post('slug', Converter::slug(Method::post('name')));
            Method::post('date', Date::set('{year}-{monthNumber}-{dayNumber} {hour}:{minute}:{second}'));

            if( $this->category->insert('post') )
			{
				$this->view->success = 'Your category has been successfully created.';
			}
			else
			{
				$this->view->error   = 'Your category could not be created. Try again later!';
			}
        }

        $this->masterpage->title = 'Add Category';
    }

    public function edit(Int $id)
    {
        if( Method::post('update') )
        {
            Method::post('slug', Converter::slug(Method::post('name')));
            
            if( $this->category->updateId('post', $id) )
			{
				$this->view->success = 'Your category has been successfully updated.';
			}
			else
			{
				$this->view->error   = 'Your category could not be updated. Try again later!';
			}
        }

        $this->masterpage->title = 'Edit Category';
        $this->view->row = $this->category->rowId($id);
    }

    public function delete(Int $id)
    {
        $this->category->deleteId($id);

        redirect( prevUrl() );
    }
}
