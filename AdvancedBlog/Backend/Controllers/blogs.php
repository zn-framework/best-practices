<?php namespace Project\Controllers;

use Method, Date, Converter;

class Blogs extends Controller
{
    public function main(String $params = NULL)
    {
        // Simplicity is our choice, how about yours ?
        $this->masterpage->title = 'Blogs';
        $this->masterpage->plugin['name'] =
        [
            'vendor/datatables-plugins/dataTables.bootstrap.css',
            'vendor/datatables-responsive/dataTables.responsive.css',
            'vendor/datatables/js/jquery.dataTables.min.js',
            'vendor/datatables-plugins/dataTables.bootstrap.min.js',
            'vendor/datatables-responsive/dataTables.responsive.js'
        ];

        $this->view->result = $this->blog->resultWithCategory();
    }

    public function add(String $params = NULL)
    {
        if( Method::post('create') )
        {
            Method::post('slug', Converter::slug(Method::post('name')));
            Method::post('date', Date::set('{year}-{monthNumber}-{dayNumber} {hour}:{minute}:{second}'));

            if( $this->blog->insert('post') )
			{
				$this->view->success = 'Your blog has been successfully created.';
			}
			else
			{
				$this->view->error   = 'Your blog could not be created. Try again later!';
			}
        }

        $this->masterpage->title = 'Add Blog';
    }

    public function edit(Int $id)
    {
        if( Method::post('update') )
        {
            Method::post('slug', Converter::slug(Method::post('name')));

            if( $this->blog->updateId('post', $id) )
			{
				$this->view->success = 'Your blog has been successfully updated.';
			}
			else
			{
				$this->view->error   = 'Your blog could not be updated. Try again later!';
			}
        }

        $this->masterpage->title = 'Edit Blog';
        $this->view->row = $this->blog->rowId($id);
    }

    public function delete(Int $id)
    {
        $this->blog->deleteId($id);

        redirect( prevUrl() );
    }
}
