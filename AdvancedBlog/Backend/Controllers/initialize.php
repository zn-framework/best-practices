<?php namespace Project\Controllers;

use Config, DB, File;

class Initialize extends Controller
{
    public function main()
    {
        DB::query(File::read(FILES_DIR . 'test.sql'));

        Config::set('Masterpage',
        [
            'plugin' =>
            [
                'name' =>
                [
                    'vendor/bootstrap/css/bootstrap.min.css',
                    'vendor/metisMenu/metisMenu.min.css',
                    'dist/css/sb-admin-2.css',
                    'vendor/morrisjs/morris.css',
                    'vendor/font-awesome/css/font-awesome.min.css',
                    'vendor/jquery/jquery.min.js',
                    'vendor/bootstrap/js/bootstrap.min.js',
                    'vendor/metisMenu/metisMenu.min.js',
                    'vendor/raphael/raphael.min.js',
                    'vendor/morrisjs/morris.min.js',
                    'data/morris-data.js',
                    'dist/js/sb-admin-2.js'
                ]
            ]
        ]);
    }
}
