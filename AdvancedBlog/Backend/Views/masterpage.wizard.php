<!-- Projects/Backend/Config/Masterpage.php -> Body Page -->
<div id="wrapper">

@Import::view('sections/header.wizard'):

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{Strings::titleCase(CURRENT_CONTROLLER)}}</h1>
            </div>

        </div>

        {{$view}}

    </div>

</div>
