<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                DataTables Advanced Tables
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th>Message</th>
                            <th>IP</th>
                            <th>Process</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $result as $row ):
                        <tr class="odd gradeX">
                            <td>{{$row->name}}</td>
                            <td>{{$row->email}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->message}}</td>
                            <td>{{$row->ip}}</td>
                            <td>
                                @@Html::anchor('tickets/delete/' . $row->id, '<span><i class="fa fa-trash-o fa-fw"></i></span>'):
                            </td>
                        </tr>
                        @endforeach:
                    </tbody>
                </table>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<script>
$(document).ready(function() {
    $('#dataTables-example').DataTable({
        responsive: true
    });
});
</script>
