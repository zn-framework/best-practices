<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Category Form
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        @@Form::open():

                            <div class="form-group">
                                <label>Name</label>
                                @@Form::required()->class('form-control')->text('name', $row->name):
                                <p class="help-block">Blog Name/Title</p>
                            </div>

                            <div class="form-group">
                                <label>Description</label>
                                @@Form::class('form-control')->text('description', $row->description):
                                <p class="help-block">Blog Description</p>
                            </div>

                            @@returnInfo($success ?? NULL, $error ?? NULL):

                            @@Form::class('btn btn-default')->submit('update', 'UPDATE'):
                            @@Form::class('btn btn-default')->reset('reset', 'RESET'):

                        @@Form::close():
                    </div>

                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
