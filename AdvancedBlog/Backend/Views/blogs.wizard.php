<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                DataTables Advanced Tables
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Name</th>
                            <th>Content</th>
                            <th>Image</th>
                            <th>Date</th>
                            <th>Process</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $result as $row ):
                        <tr class="odd gradeX">
                            <td>{{$row->categoryName}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{Limiter::char($row->content, 200)}}</td>
                            <td>{{$row->image}}</td>
                            <td>{{$row->date}}</td>
                            <td>
                                @@Html::anchor('blogs/edit/' . $row->id, '<span><i class="fa fa-edit fa-fw"></i></span>'):
                                @@Html::anchor('blogs/delete/' . $row->id, '<span><i class="fa fa-trash-o fa-fw"></i></span>'):
                            </td>
                        </tr>
                        @endforeach:
                    </tbody>
                </table>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<script>
$(document).ready(function() {
    $('#dataTables-example').DataTable({
        responsive: true
    });
});
</script>
