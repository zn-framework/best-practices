<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Blog Form
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        @@Form::open():
                            <div class="form-group">
                                <label>Categories</label>
                                @@Form::required()->class('form-control')->table('category')->select('category_id', ['id' => 'name']):
                                <p class="help-block">Select Category</p>
                            </div>

                            <div class="form-group">
                                <label>Name</label>
                                @@Form::required()->class('form-control')->text('name'):
                                <p class="help-block">Blog Name/Title</p>
                            </div>

                            <div class="form-group">
                                <label>Image</label>
                                @@Form::class('form-control')->text('image'):
                                <p class="help-block">Image Path</p>
                            </div>

                            <div class="form-group">
                                <label>Content</label>
                                @@Form::required()->class('form-control')->textarea('content'):
                            </div>

                            @@returnInfo($success ?? NULL, $error ?? NULL):

                            @@Form::class('btn btn-default')->submit('create', 'CREATE'):
                            @@Form::class('btn btn-default')->reset('reset', 'RESET'):

                        @@Form::close():
                    </div>

                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
