<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                DataTables Advanced Tables
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Process</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $result as $row ):
                        <tr class="odd gradeX">
                            <td>{{$row->name}}</td>
                            <td>{{$row->description}}</td>
                            <td>{{$row->date}}</td>
                            <td>
                                @@Html::anchor('categories/edit/' . $row->id, '<span><i class="fa fa-edit fa-fw"></i></span>'):
                                @@Html::anchor('categories/delete/' . $row->id, '<span><i class="fa fa-trash-o fa-fw"></i></span>'):
                            </td>
                        </tr>
                        @endforeach:
                    </tbody>
                </table>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<script>
$(document).ready(function() {
    $('#dataTables-example').DataTable({
        responsive: true
    });
});
</script>
