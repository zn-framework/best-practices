<?php namespace Project\Controllers;

class Home extends Controller
{
    public function main(String $params = NULL)
    {
        // Simplicity is our choice, how about yours ?

        $this->masterpage->title = 'Basic Theme Integration';
        $this->masterpage->page  = 'home.wizard';
    }
}
