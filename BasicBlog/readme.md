# Version Info

ZN >= 4.5.0

# Links

List: domainname/<br>
Add : domainname/add-book<br>
Edit: domainname/edit-book/{{id}}<br>

# Used Structures

Migration<br>
GrandModel<br>
Template Wizard<br>
Method Library<br>
Masterpage<br>
Route

# Usage

* Run: MigrateBooks::up() from Terminal or Devtools